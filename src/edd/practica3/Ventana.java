package edd.practica3;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class Ventana {

	private JFrame frame;
	private JTextField cadena;
	private JLabel lblIngreseLaCadena;
	private JButton btnInsertar;
	private JButton btnEliminar;
	private JLabel lblConsola;
	private JScrollPane scrollPane;
	public static JTextArea output;
	private TablaHash myHashTable;
	private JLabel lblTablaHash;
	private JScrollPane scrollPane2;
	private JTextArea contenidoTabla;
	private JLabel lblContenidotabla;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ventana() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frame = new JFrame();
		this.frame.setBounds(100, 100, 690, 312);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setTitle("Hash Table por JULIAN CHAMALE");
		myHashTable = new TablaHash(1699, TablaHash.DIRECCIONAMIENTO_CERRADO);

		this.cadena = new JTextField();
		this.cadena.setColumns(10);

		this.lblIngreseLaCadena = new JLabel("Ingrese la cadena");

		this.btnInsertar = new JButton("Insertar");
		this.btnInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cadena.getText().length() > 0
						&& myHashTable.add(cadena.getText()) != -1)
					contenidoTabla.setText(myHashTable.toString());
			}
		});

		this.btnEliminar = new JButton("Eliminar");
		this.btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cadena.getText().length() > 0
						&& myHashTable.delete(cadena.getText()) != -1)
					contenidoTabla.setText(myHashTable.toString());
			}
		});

		this.lblConsola = new JLabel("Consola");

		this.scrollPane = new JScrollPane();

		this.lblTablaHash = new JLabel("TABLA HASH");

		this.scrollPane2 = new JScrollPane();

		this.lblContenidotabla = new JLabel("ContenidoTabla");
		GroupLayout groupLayout = new GroupLayout(this.frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(19)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(this.scrollPane, GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE)
						.addComponent(this.lblConsola)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(this.btnInsertar)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(this.btnEliminar))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(this.lblIngreseLaCadena)
							.addGap(46)
							.addComponent(this.lblTablaHash))
						.addComponent(this.cadena, GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(this.lblContenidotabla)
						.addComponent(this.scrollPane2, GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(this.lblIngreseLaCadena)
								.addComponent(this.lblContenidotabla)))
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(this.lblTablaHash)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(this.cadena, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(this.btnEliminar)
								.addComponent(this.btnInsertar))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(this.lblConsola)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(this.scrollPane, GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE))
						.addComponent(this.scrollPane2, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))
					.addGap(17))
		);

		this.contenidoTabla = new JTextArea();
		this.scrollPane2.setViewportView(this.contenidoTabla);

		output = new JTextArea();
		output.setEditable(false);
		this.scrollPane.setViewportView(output);
		this.frame.getContentPane().setLayout(groupLayout);

		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			SwingUtilities.updateComponentTreeUI(frame);
		} catch (Exception exc) {
			System.out.println("Ocurrió un error");
		}
	}
}
