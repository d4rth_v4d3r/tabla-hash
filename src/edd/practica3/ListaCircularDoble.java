/**
 * 
 */
package edd.practica3;

import java.util.Iterator;

/**
 * @author julian
 * 
 */
public class ListaCircularDoble implements Iterable<NodoLista> {
	private NodoLista inicio;
	private int length;

	/**
	 * Esta lista tiene una politica FIFO
	 */
	public ListaCircularDoble() {
		this.inicio = null;
		this.length = 0;
	}

	/**
	 * @return the inicio
	 */
	public NodoLista getInicio() {
		return inicio;
	}

	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	public boolean isEmpty() {
		return this.inicio == null;
	}

	private void insertar(Object dato, NodoLista target) {
		if (target == null) {
			inicio = new NodoLista(dato);
			this.inicio.setSiguiente(inicio);
			this.inicio.setAnterior(inicio);
		}
	}

	/**
	 * Este metodo inserta nodos al final de la lista, sin importar si estan
	 * repetidos
	 * 
	 * @param dato
	 *            El dato a insertar
	 */
	public void add(Object dato) {
		if (this.isEmpty())
			this.insertar(dato, null);
		else {
			new NodoLista(dato, this.inicio.getAnterior(), this.inicio);
		}
		this.length++;
	}

	/**
	 * Este metodo borra el blanco
	 * 
	 * @param key
	 *            El blanco a eliminar
	 */
	public int delete(Object target) {
		for (NodoLista auxiliar : this) {
			if (auxiliar.compare(target)) {
				// if ( auxiliar.compareTo( target ) == 0 ) {
				NodoLista anteriorAux = auxiliar.getAnterior();
				NodoLista siguienteAux = auxiliar.getSiguiente();
				if (auxiliar == this.inicio)
					this.inicio = siguienteAux;
				anteriorAux.setSiguiente(siguienteAux);
				siguienteAux.setAnterior(anteriorAux);

				auxiliar = null;
				length--;
				return 0;
			}

		}
		return -1;
	}

	/**
	 * Este metodo busca el blanco especificado
	 * 
	 * @param target
	 *            El blanco a buscar
	 * @return El nodo que contiene al blanco o null si no existe en la lista
	 */
	public NodoLista get(Object target) {
		for (NodoLista auxiliar : this) {
			if (auxiliar.compare(target))
				return auxiliar;
		}

		return null;
	}

	public void clear() {
		this.inicio = null;
		this.length = 0;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer ts = new StringBuffer();
		for (NodoLista nodo : this) {
			ts.append(nodo.toString() + " -> ");
		}
		return ts.toString();
	}

	@Override
	public Iterator<NodoLista> iterator() {
		// TODO Auto-generated method stub
		return new IteradorLista();
	}

	private class IteradorLista implements Iterator<NodoLista> {
		private int ptr = 0;
		private NodoLista aux = inicio;

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return ptr < length;
		}

		@Override
		public NodoLista next() {
			// TODO Auto-generated method stub
			++ptr;
			aux = aux.getSiguiente();
			return aux;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			throw new UnsupportedOperationException();
		}

	}
}
