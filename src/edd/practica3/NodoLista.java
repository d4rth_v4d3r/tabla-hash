/**
 * 
 */
package edd.practica3;

/**
 * @author julian
 * 
 */
public class NodoLista implements Comparable<Object> {
	private NodoLista siguiente, anterior;
	private Object dato;

	/**
	 * 
	 */
	public NodoLista(Object dato) {
		// TODO Auto-generated constructor stub
		this.setDato(dato);
	}

	/**
	 * 
	 * @param dato
	 *            El dato a almacenar
	 * @param anterior
	 *            El nodo anterior
	 * @param siguiente
	 *            El nodo siguiente
	 */
	public NodoLista(Object dato, NodoLista anterior, NodoLista siguiente) {
		// TODO Auto-generated constructor stub
		this.setDato(dato);
		this.setSiguiente(siguiente);
		siguiente.setAnterior(this);
		this.setAnterior(anterior);
		anterior.setSiguiente(this);
	}

	/**
	 * @return the siguiente
	 */
	public NodoLista getSiguiente() {
		return siguiente;
	}

	/**
	 * @param siguiente
	 *            the siguiente to set
	 */
	public void setSiguiente(NodoLista siguiente) {
		this.siguiente = siguiente;
	}

	/**
	 * @return the anterior
	 */
	public NodoLista getAnterior() {
		return anterior;
	}

	/**
	 * @param anterior
	 *            the anterior to set
	 */
	public void setAnterior(NodoLista anterior) {
		this.anterior = anterior;
	}

	/**
	 * @return the dato
	 */
	public Object getDato() {
		return dato;
	}

	/**
	 * @param dato
	 *            the dato to set
	 */
	public void setDato(Object dato) {
		this.dato = dato;
	}

	/**
	 * Este metodo compara el blanco especificado con el dato del nodo, para
	 * reutilizar el codigo solo se necesita reescribir este metodo y
	 * sobreescribir los toString().
	 * 
	 * @param target
	 *            El blanco a comparar
	 * @return 0 si el blanco es igual al dato, de otro modo -1;
	 */
	public int compareTo(Object target) {
		return StringHandler.clearString(dato.toString()).compareTo(
				StringHandler.clearString(target.toString()));
	}

	public boolean compare(Object target) {
		return this.dato.toString().equals(target.toString());
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.dato == null ? "<null>" : this.dato.toString();
	}
}
