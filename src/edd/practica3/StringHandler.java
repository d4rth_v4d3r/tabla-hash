package edd.practica3;
/**
 * 
 */

import java.text.Collator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author julian
 * 
 */
public final class StringHandler {

	public static String clearString( String cadena ) {
		return clearTildes( clearBlankSpaces( replaceBlankSpaces( clearChars( cadena ) ) ) )
				.toLowerCase();
	}

	public static String replaceBlankSpaces( String cadena ) {
		return cadena.replaceAll( "\\s+", "_" );
	}

	public static String clearBlankSpaces( String cadena ) {
		return cadena.trim();
	}

	public static String clearTildes( String cadena ) {
		String cleanString = "";
		Collator comparador = Collator.getInstance();
		comparador.setStrength( Collator.PRIMARY );
		for ( int i = 0; i < cadena.length(); i++ ) {
			String caracter = String.valueOf( cadena.charAt( i ) );
			if ( comparador.compare( caracter, "a" ) == 0 )
				cleanString += "a";
			else if ( comparador.compare( caracter, "e" ) == 0 )
				cleanString += "e";
			else if ( comparador.compare( caracter, "i" ) == 0 )
				cleanString += "i";
			else if ( comparador.compare( caracter, "o" ) == 0 )
				cleanString += "o";
			else if ( comparador.compare( caracter, "u" ) == 0 )
				cleanString += "u";
			else
				cleanString += caracter;
		}

		return cleanString;
	}
	
	public static String clearChars( String cadena ) {
		Pattern p = Pattern.compile( "\\W+" );
		Matcher m = p.matcher( cadena.trim() );
		if ( m.find() ) {
			cadena = m.replaceAll( "_" );
			return cadena;
		}
		return cadena;
	}

	public static boolean check( String cadena ) {
		Pattern p = Pattern.compile( "\\W+" );
		Matcher m = p.matcher( cadena.trim() );
		if ( m.find() ) {
			cadena = m.replaceAll( "_" );
			return false;
		}
		return true;
	}
}
