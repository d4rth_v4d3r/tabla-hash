/**
 * 
 */
package edd.practica3;

/**
 * @author julian
 * 
 */
public class TablaHash {
	private Object[] tabla;
	private ListaCircularDoble ocupados;
	public static final int DIRECCIONAMIENTO_CERRADO = 0,
			EXPLORACION_LINEAL = 1, EXPLORACION_CUADRATICA = 2,
			DOBLE_DIRECCION_DISPERSA = 3, INSERTAR = 0;
	private int politicaColisiones, x;

	/**
	 * 
	 */
	public TablaHash(int tamano_inicial, int politicaColisiones) {
		// TODO Auto-generated constructor stub
		ocupados = new ListaCircularDoble();
		this.tabla = new Object[tamano_inicial];
		this.x = tamano_inicial;
		this.politicaColisiones = politicaColisiones;
	}

	/**
	 * @return the tabla
	 */
	public Object[] getTabla() {
		return tabla;
	}

	public void reset() {
		this.tabla = new Object[this.x];
	}

	/**
	 * Calcule la posicion de un objeto en la tabla Hash
	 * 
	 * @param m
	 *            El entero asociado con el objeto, use la funcion getInt() para
	 *            obtener m.
	 * @return La posicion asociada al objeto.
	 */
	public int hashFunction(Object dato) {
		int m = this.getInt(dato, dato.toString().length() - 1), p = m % this.x, i = 0, k = 0;
		switch (politicaColisiones) {
		case DIRECCIONAMIENTO_CERRADO:
			return m % this.x;
		case EXPLORACION_LINEAL:
			while (true) {
				k = p + i;
				if (k > tabla.length - 1)
					k = k % this.x;
				if (this.tabla[k] == null)
					return k;
				else if (this.tabla[k].toString().compareTo(dato.toString()) == 0)
					return k;
				else
					i++;
			}
		case EXPLORACION_CUADRATICA:
			while (true) {
				k = p + i * i;
				if (k > tabla.length - 1)
					k = k % this.x;
				if (this.tabla[k] == null)
					return k;
				else if (this.tabla[k].toString().compareTo(dato.toString()) == 0)
					return k;
				else
					i++;
			}
		case DOBLE_DIRECCION_DISPERSA: // Sobreescribir esta dispersion con la
										// nueva funcion.
			return p + 1;
		default:
			return p;
		}

	}

	private int insertar(Object dato, int k) {
		switch (this.politicaColisiones) {
		case DIRECCIONAMIENTO_CERRADO:
			if (this.tabla[k] == null) {
				this.tabla[k] = dato;
				return 0;
			} else if (this.tabla[k] instanceof ListaCircularDoble) {
				ListaCircularDoble colision = (ListaCircularDoble) this.tabla[k];
				if (colision.get(dato) == null) {
					colision.add(dato);
					return 0;
				} else
					return -1;
			} else {
				ListaCircularDoble colision = new ListaCircularDoble();
				colision.add(this.tabla[k]);
				if (colision.get(dato) == null) {
					this.tabla[k] = colision;
					colision.add(dato);
					return 0;
				} else
					return -1;
			}
		default:
			if (this.tabla[k] == null) { // la tabla en esta posicion solo
											// puede
				// retornar nulo o el mismo valor, osea
				// que ya existe
				this.tabla[k] = dato;
				this.ocupados.add(k);
				return 0;
			} else
				return -1;
		}
	}

	/**
	 * Este metodo recalcula las claves, expandiendo el tamaño de la matriz.
	 */
	public void reHash() {
		this.x = Math.round(this.tabla.length * 1.5f);
		Object[] oldHash = this.tabla;
		this.tabla = new Object[this.x];
		ListaCircularDoble nuevoOcupados = new ListaCircularDoble();

		for (NodoLista auxiliar : nuevoOcupados) {
			int k_1 = Integer.valueOf(auxiliar.getDato().toString());
			int k_2 = hashFunction(oldHash[k_1]); // recalculo de
													// claves
			this.tabla[k_2] = oldHash[k_1];
			nuevoOcupados.add(k_2);
		}

		this.ocupados = nuevoOcupados;
	}

	/**
	 * Este metodo retorna un entero asociado al objeto dato para luego
	 * aplicarlo a la funcion hash
	 * 
	 * @param dato
	 *            El objeto al cual se le calculara su coeficiente m.
	 * @return El entero asociado.
	 */
	public int getInt(Object dato, int position) {
		if (position == 0)
			return 1;
		else {
			int c = (int) dato.toString().charAt(position);
			return c * ((getInt(dato, position - 1) % 257) + 1);
		}
	}

	public int add(Object dato) {
		int k = this.hashFunction(dato);
		k = this.insertar(dato, k) == 0 ? k : -1;
		if (k != -1) {
			if (this.ocupados.get(k) == null)
				this.ocupados.add(k);
			Ventana.output.setText(Ventana.output.getText()
					+ String.format("Insercion correcta en %d.\n", k));
		} else {
			Ventana.output.setText(Ventana.output.getText()
					+ String.format("Elemento ya existe.\n", k));
		}
		return k;
		// float carga = this.ocupados.getLength() * 1.00f / this.tabla.length;
		// if ( carga > 0.60 )
		// reHash();
	}

	/**
	 * Este metodo busca el dato en la tabla y si lo encuentra lo retorna.
	 * 
	 * @param dato
	 *            El dato a buscar
	 * @return El objeto si lo encuentra o null en otro caso.
	 */
	public Object get(Object dato) {
		int k = this.hashFunction(dato);
		if (dato instanceof ListaCircularDoble)
			return ((ListaCircularDoble) this.tabla[k]).get(dato);
		else
			return this.tabla[k];
	}

	/**
	 * Este metodo borra el dato si lo encuentra.
	 * 
	 * @param dato
	 *            El dato a buscar.
	 * @return La posicion si existe el dato y lo borra o -1 si no lo encuentra.
	 */
	public int delete(Object dato) {
		int k = this.hashFunction(dato);
		if (this.tabla[k] instanceof ListaCircularDoble) {
			k = ((ListaCircularDoble) this.tabla[k]).delete(dato) == 0 ? k : -1;
			if (k != -1) {
				if (((ListaCircularDoble) this.tabla[k]).getLength() == 1)
					this.tabla[k] = ((ListaCircularDoble) this.tabla[k])
							.getInicio().getDato();
			}

		} else if (this.tabla[k] == null)
			k = -1;
		else {
			if (this.tabla[k].toString().equals(dato.toString())) {
				this.tabla[k] = null;
				this.ocupados.delete(k);
			} else
				k = -1;
		}

		if (k == -1) {
			Ventana.output.setText(Ventana.output.getText()
					+ String.format("No se encontro el elemento.\n", k));
		} else {
			Ventana.output.setText(Ventana.output.getText()
					+ String.format("Eliminacion correcta de %d.\n", k));
		}
		return k;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer();
		for (NodoLista nodo : this.ocupados) {
			Object dato = this.tabla[(Integer) nodo.getDato()];
			sb.append(String.format("%-7s:%-4s%s%s", nodo.getDato(), "", dato,
					System.getProperty("line.separator")));
		}
		return sb.toString();
	}
}
